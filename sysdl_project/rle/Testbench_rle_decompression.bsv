import rle_decompression::*;

module mkTestbench();

	Reg#(Bit#(4)) rg_flag <- mkReg(0);
	Reg#(Bit#(6)) rg_counter <- mkReg(0);
	
	Ifc_rle_decompression#(16,8) rle_decompress <- mkrle_decompression;

	rule rl_0(rg_flag == 0);
		if(rg_counter == 4)
			rle_decompress.ma_transmission_done;
		else
			rle_decompress.ma_get_inputs(zeroExtend(rg_counter), zeroExtend(rg_counter));
		rg_counter <= rg_counter + 1;
	endrule
	rule rl_1(rg_flag == 0);
		let temp <- rle_decompress.mav_output;
		$display($stime,"Output : %d \n", temp);
	endrule
	rule rl_2(rle_decompress.mv_decompression_done == True);
		$finish;
	endrule

endmodule
