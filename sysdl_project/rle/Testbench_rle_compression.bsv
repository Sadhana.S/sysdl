import rle_compression::*;

module mkTestbench();
	
	Reg#(Bit#(2)) rg_flag <- mkReg(0);
	Reg#(Bit#(6)) rg_counter <- mkReg(0);

	Ifc_rle_compression#(16,8) rle_compress <- mkrle_compression;

	rule rl_0(rg_flag == 0);
		rle_compress.ma_start_compression(True);
		rg_flag <= 1;
	endrule
	rule rl_1((rg_flag == 1)&&(rg_counter < 32));
		if(rg_counter < 16)
			rle_compress.ma_input_to_compress(1);
		else if(rg_counter < 24)
			rle_compress.ma_input_to_compress(2);
		else if(rg_counter < 25)
			rle_compress.ma_input_to_compress(4);
		else if(rg_counter < 28)
			rle_compress.ma_input_to_compress(3);
		else
			rle_compress.ma_input_to_compress(0);
		rg_counter <= rg_counter + 1;
	endrule
	rule rl_2(rg_counter > 0);
		if(rle_compress.mv_compression_out_ready == True) begin
			let temp <- rle_compress.mav_compressed_value;
			let temp_1 = tpl_1(temp);
			let temp_2 = tpl_2(temp);
			$display($time," Compressed value : %d, %d \n",temp_1,temp_2);
		end
	endrule
	rule rl_3(rg_counter == 32);
		rle_compress.ma_all_inputs_transmitted(True);
		rg_flag <= 2;
	endrule
	rule rl_4(rg_flag == 2);
		$finish;
	endrule
endmodule
