package rle_decompression;

	interface Ifc_rle_decompression#(numeric type word_width, numeric type counter_width);
		method Action ma_get_inputs(Bit#(word_width) word_in, Bit#(counter_width) count_in);
		method Action ma_transmission_done;
		method ActionValue#(Bit#(word_width)) mav_output;
		method Bool mv_decompression_done;
	endinterface : Ifc_rle_decompression

	module mkrle_decompression(Ifc_rle_decompression#(word_width,counter_width));
		
		Reg#(Bit#(counter_width)) rg_counter <- mkReg(0);
		Reg#(Bit#(word_width)) rg_curr_word <- mkReg(0);
		Reg#(Bool) rg_transmit <- mkReg(False);
		Reg#(Bool) rg_end <- mkReg(False);

		method Action ma_get_inputs(Bit#(word_width) word_in, Bit#(counter_width) count_in) if(rg_transmit == False);
			rg_curr_word <= word_in;
			rg_counter <= count_in;
			rg_transmit <= True;
			rg_end <= False;
		endmethod
		method Action ma_transmission_done;
			rg_end <= True;
		endmethod
		method ActionValue#(Bit#(word_width)) mav_output if(rg_transmit == True);
			rg_counter <= rg_counter-1;
			if(rg_counter == 0)
				rg_transmit <= False;
			return rg_curr_word;
		endmethod
		method Bool mv_decompression_done if(rg_end == True && rg_transmit == False);
			return True;
		endmethod
	endmodule : mkrle_decompression

endpackage : rle_decompression
