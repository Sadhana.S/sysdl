package rle_compression;

	interface Ifc_rle_compression#(numeric type word_width, numeric type compression_width);
		method Action ma_start_compression(Bool in);
		method Action ma_input_to_compress(Bit#(word_width) in);
		method Action ma_all_inputs_transmitted(Bool in);
		method ActionValue#(Tuple2#(Bit#(word_width), Bit#(compression_width))) mav_compressed_value;
		method Bool mv_compression_out_ready;
	endinterface : Ifc_rle_compression

	module mkrle_compression(Ifc_rle_compression#(word_width, compression_width));

		Integer max_count = valueOf(TSub#(TExp#(compression_width),1));

		Reg#(Bool) rg_start_compression <- mkReg(False);
		Reg#(Bool) rg_next_compression <- mkReg(False);
		Reg#(Bit#(word_width)) rg_curr_word <- mkReg(0);
		Reg#(Bit#(word_width)) rg_prev_word <- mkReg(0);
		Reg#(Bit#(compression_width)) rg_counter <- mkReg(0);

		method Action ma_start_compression(Bool in);
			rg_start_compression <= True;
		endmethod
		method Action ma_input_to_compress(Bit#(word_width) in) if(rg_next_compression == False);
			if(rg_start_compression == True) begin
				rg_curr_word <= in;
				rg_start_compression <= False;
			end
			else begin
				if(rg_curr_word != in) begin
					rg_curr_word <= in;
					rg_next_compression <= True;
				end
				else begin
					rg_prev_word <= rg_curr_word;
					if(rg_counter == fromInteger(max_count))
						rg_next_compression <= True;
					else
						rg_counter <= rg_counter + 1;
				end
			end
			$display($time," Input : %d ; Counter : %d  \n",in,rg_counter);
		endmethod
		method Action ma_all_inputs_transmitted(Bool in);
			rg_next_compression <= in;
		endmethod
		method ActionValue#(Tuple2#(Bit#(word_width), Bit#(compression_width))) mav_compressed_value if(rg_next_compression == True);
			let temp = tuple2(rg_prev_word, rg_counter);
			rg_counter <= 0;
			rg_next_compression <= False;
			rg_prev_word <= rg_curr_word;
			return temp;
		endmethod
		method Bool mv_compression_out_ready;
			return rg_next_compression;
		endmethod

	endmodule : mkrle_compression

endpackage : rle_compression
