#include <stdlib.h>
#include <iostream>
#include "util.h"
#include <immintrin.h>

Convolution::Convolution(int m, int c, int r, int s, int sx, int sy, int px, int py)
{
  M = m;
  C = c;
  R = r;
  S = s;
  Sx = sx;
  Sy = sy;
  Px = px;
  Py = py;
  weights = (DATA*) malloc(M * C * R * S * sizeof(DATA));
  DATA (*temp)[C][R][S] = (DATA (*)[C][R][S])weights;
  for(int i=0; i<M; i++)
    for(int j=0; j<C; j++)
      for(int k=0; k<R; k++)
        for(int l=0; l<S; l++)
          temp[i][j][k][l] = (i*C*R*S+j*R*S+k*S+l)%256;
}

//Order of weight storage is changed
Linear::Linear(int m, int l)
{
  M = m;
  L = l;
  weights = (DATA*) malloc(M * L * sizeof(DATA));
  DATA (*temp)[M] = (DATA (*)[M])weights;
  for(int i=0; i<M; i++)
    for(int j=0; j<L; j++)
      temp[j][i] = (i*L+j)%256;
}
fmap* Convolution::conv_2d(fmap* input_features)
{
	int N = input_features->dim1;
	int H = input_features->dim3;
	int W = input_features->dim4;
  std::cout << " \n Conv_2d Dimensions of ifmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  std::cout << " \n Conv_2d Dimensions of weights \n \t M = " << M << "\t C = " << C << "\t R = " << R << "\t S = " << S << "\n";
	int E = ((H - R + 2*Px)/Sx) + 1;
	int F = ((W - S + 2*Py)/Sy) + 1;
	fmap* out_fmap = new fmap;
	out_fmap->dim1 = N;
	out_fmap->dim2 = M;
	out_fmap->dim3 = E;
	out_fmap->dim4 = F;
	out_fmap->data = (DATA*)calloc(N * M * E * F, sizeof(float));
	float (*temp_ofmap)[M][E][F] = (float (*)[M][E][F])out_fmap->data;
  float (*temp_weights)[C][R][S] = (float (*)[C][R][S])weights;
  float (*temp_ifmap)[C][H][W] = (float (*)[C][H][W])input_features->data;

	__m256i mem_mask_weight, mem_mask_ifmap;
	__m256 temp_1_weight, temp_1_ifmap, temp_out;
	int index_weight,index_ifmap[8];

  clock_t start, end;
  start = clock();
  for(int n=0; n<N; n++)
  	for(int m=0; m<M; m++)
  		for(int c=0; c<C; c++)
  			for(int e=0; e<E; e++)
  				for(int f=0; f<F; f++)
  					for(int r=0; r<R; r++) {
  						int temp_r = e*Sx+r-Px;
  						if((temp_r >= 0)&&(temp_r < E))
  							for(int s=0; s<S; s=s+8) {
  								int temp_c = f*Sy+s-Py;
									for(int count=0; count<8; count++) {
  									if((f*Sy+s+count-Py >= 0)&&(f*Sy+s+count-Py < F))
											index_ifmap[count] = s-S;
										else
											index_ifmap[count] = S;
									}
									index_weight = s-S;
									mem_mask_weight = _mm256_set_epi32(index_weight+7,index_weight+6,index_weight+5,index_weight+4,index_weight+3,index_weight+2,index_weight+1,index_weight);
									mem_mask_ifmap = _mm256_set_epi32(index_ifmap[7],index_ifmap[6],index_ifmap[5],index_ifmap[4],index_ifmap[3],index_ifmap[2],index_ifmap[1],index_ifmap[0]);
									temp_1_weight = _mm256_maskload_ps((float const*) &temp_weights[m][c][r][s], mem_mask_weight);
									temp_1_ifmap  = _mm256_maskload_ps((float const*) &temp_ifmap[n][c][temp_r][temp_c], mem_mask_ifmap);
									temp_out = _mm256_mul_ps(temp_1_weight, temp_1_ifmap);
									temp_out = _mm256_add_ps(temp_out,_mm256_maskload_ps((float const*) &temp_ofmap[n][m][e][f], mem_mask_ifmap));
									_mm256_maskstore_ps(&temp_ofmap[n][m][e][f],mem_mask_ifmap,temp_out);
								}
  					}
  end = clock(); 
  exec_time = double(end-start) / double(CLOCKS_PER_SEC);
  std::cout << " \n Conv_2d Dimensions of Ofmap \n \t N = " << N << "\t M = " << M << "\t E = " << E << "\t F = " << F << "\n" ;
	return out_fmap;
}

fmap* Convolution::conv2d_IS(fmap* input_features)
{
	int N = input_features->dim1;
	int H = input_features->dim3;
	int W = input_features->dim4;
  std::cout << " \n Conv_2d Dimensions of ifmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  std::cout << " \n Conv_2d Dimensions of weights \n \t M = " << M << "\t C = " << C << "\t R = " << R << "\t S = " << S << "\n";
	int E = ((H - R + 2*Px)/Sx) + 1;
	int F = ((W - S + 2*Py)/Sy) + 1;
	fmap* out_fmap = new fmap;
	out_fmap->dim1 = N;
	out_fmap->dim2 = M;
	out_fmap->dim3 = E;
	out_fmap->dim4 = F;
	out_fmap->data = (DATA*)calloc(N * M * E * F, sizeof(float));
	float (*temp_ofmap)[M][E][F] = (float (*)[M][E][F])out_fmap->data;
  float (*temp_weights)[C][R][S] = (float (*)[C][R][S])weights;
  float (*temp_ifmap)[C][H][W] = (float (*)[C][H][W])input_features->data;

	__m256i mem_mask_ifmap, mem_mask_weight;
	__m256 temp_1_ifmap, temp_1_weight, temp_1_ofmap, temp_1_accum;
	int index_ifmap;
	int index_weight[8];

  clock_t start, end;
  start = clock();
	for(int n=0; n<N; n++)
		for(int c=0; c<C; c++)
			for(int h=0; h<H+Px; h=h+Sx)
				if(h>=Px) {
					for(int w=0; w<W+Py; w=w+8*Sy)
						if(w>=Py) {
							index_ifmap = w-Py-W;
							mem_mask_ifmap = _mm256_set_epi32(index_ifmap+7, index_ifmap+6, index_ifmap+5, index_ifmap+4, index_ifmap+3, index_ifmap+2, index_ifmap+1, index_ifmap);
							temp_1_ifmap = _mm256_set_ps(temp_ifmap[n][c][h-Px][w-Py+7*Sy], temp_ifmap[n][c][h-Px][w-Py+6*Sy], temp_ifmap[n][c][h-Px][w-Py+5*Sy], temp_ifmap[n][c][h-Px][w-Py+4*Sy], 
																					 temp_ifmap[n][c][h-Px][w-Py+3*Sy], temp_ifmap[n][c][h-Px][w-Py+2*Sy], temp_ifmap[n][c][h-Px][w-Py+1*Sy], temp_ifmap[n][c][h-Px][w-Py]);
							temp_1_ifmap = _mm256_maskload_ps((float const*) &temp_ifmap[n][c][h-Px][w-Py], mem_mask_ifmap); 
							for(int m=0; m<M; m++)
								for(int r=0; r<R; r++) {
									if((h-r >= 0)&&((h-r)%Sx == 0))
									for(int s=0; s<S; s++) {
										for(int count=0; count<8; count++) {
											if((w+count*Sy - s >= 0)&&((w+count*Sy-s)%Sy==0))
												index_weight[count] = s - S;
											else
												index_weight[count] = -S;
										}
										mem_mask_weight = _mm256_set_epi32(index_weight[7], index_weight[6], index_weight[5], index_weight[4], index_weight[3], index_weight[2], index_weight[1], index_weight[0]);
										temp_1_ofmap = _mm256_maskload_ps((float const*) &temp_ofmap[n][m][(h-r)/Sx][(w-s)/Sy], mem_mask_weight);
										temp_1_weight = _mm256_broadcast_ss((float const*) &temp_weights[m][c][r][s]);
										temp_1_accum = _mm256_mul_ps(temp_1_ifmap, temp_1_weight);
										temp_1_ofmap = _mm256_add_ps(temp_1_accum, temp_1_ofmap);
										_mm256_maskstore_ps(&temp_ofmap[n][m][(h-r)/Sx][(w-s)/Sy], mem_mask_weight, temp_1_ofmap);
									}
								}
						}
					}
  end = clock(); 
  exec_time = double(end-start) / double(CLOCKS_PER_SEC);
  std::cout << " \n Conv_2d Dimensions of Ofmap \n \t N = " << N << "\t M = " << M << "\t E = " << E << "\t F = " << F << "\n" ;
	return out_fmap;
}

fmap* Convolution::conv2d_OS(fmap* input_features)
{
	int N = input_features->dim1;
	int H = input_features->dim3;
	int W = input_features->dim4;
  std::cout << " \n Conv_2d_OS Dimensions of ifmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  std::cout << " \n Conv_2d_OS Dimensions of weights \n \t M = " << M << "\t C = " << C << "\t R = " << R << "\t S = " << S << "\n";
	int E = ((H - R + 2*Px)/Sx) + 1;
	int F = ((W - S + 2*Py)/Sy) + 1;
	fmap* out_fmap = new fmap;
	out_fmap->dim1 = N;
	out_fmap->dim2 = M;
	out_fmap->dim3 = E;
	out_fmap->dim4 = F;
	out_fmap->data = (DATA*)calloc(N * M * E * F, sizeof(float));
	float (*temp_ofmap)[M][E][F] = (float (*)[M][E][F])out_fmap->data;
  float (*temp_weights)[C][R][S] = (float (*)[C][R][S])weights;
  float (*temp_ifmap)[C][H][W] = (float (*)[C][H][W])input_features->data;

	__m256i mem_mask, mem_mask_ofmap;
	__m256 temp_1_ifmap, temp_1_weight, temp_out, temp_out_fmap;
	int index[8]; 
	int index_ofmap;
	
  clock_t start, end;
  start = clock();
  for(int n=0; n<N; n++)
  	for(int m=0; m<M; m++)
  		for(int e=0; e<E; e++)
  			for(int f=0; f<F; f=f+8) {
						index_ofmap = f-F;
						mem_mask_ofmap = _mm256_set_epi32(index_ofmap+7, index_ofmap+6, index_ofmap+5, index_ofmap+4, index_ofmap+3, index_ofmap+2, index_ofmap+1, index_ofmap);
						temp_out_fmap = _mm256_loadu_ps((float const*) &temp_ofmap[n][m][e][f]);
  					for(int c=0; c<C; c++)
  					for(int r=0; r<R; r++) {
  						int temp_r = e*Sx+r-Px;
  						if((temp_r >= 0)&&(temp_r < E))
  							for(int s=0; s<S; s++) {
									int temp_c[8] = {(f+7)*Sy+s-Py, (f+6)*Sy+s-Py, (f+5)*Sy+s-Py, (f+4)*Sy+s-Py, (f+3)*Sy+s-Py, (f+2)*Sy+s-Py, (f+1)*Sy+s-Py, f*Sy+s-Py};
									for(int count=0; count<8; count++) {
										if(temp_c[count] >=0 && temp_c[count] < F)
											index[count] = s - S;
										else
											index[count] = S;
									}
									mem_mask = _mm256_set_epi32(index[7], index[6], index[5], index[4], index[3], index[2], index[1], index[0]);
									temp_1_weight = _mm256_broadcast_ss((float const*) &temp_weights[m][c][r][s]);
									temp_1_ifmap  = _mm256_set_ps(temp_ifmap[n][c][temp_r][temp_c[7]], temp_ifmap[n][c][temp_r][temp_c[6]], temp_ifmap[n][c][temp_r][temp_c[5]], temp_ifmap[n][c][temp_r][temp_c[4]],
																								temp_ifmap[n][c][temp_r][temp_c[3]], temp_ifmap[n][c][temp_r][temp_c[2]], temp_ifmap[n][c][temp_r][temp_c[1]], temp_ifmap[n][c][temp_r][temp_c[0]]);
									temp_out = _mm256_mul_ps(temp_1_weight, temp_1_ifmap);
									temp_out_fmap = _mm256_add_ps(temp_out, temp_out_fmap);
  							}
  					}
					_mm256_maskstore_ps(&temp_ofmap[n][m][e][f],mem_mask_ofmap,temp_out_fmap);
				}
  end = clock(); 
  exec_time = double(end-start) / double(CLOCKS_PER_SEC);
  std::cout << " \n Conv_2d_OS Dimensions of Ofmap \n \t N = " << N << "\t M = " << M << "\t E = " << E << "\t F = " << F << "\n" ;
	return out_fmap;
}

fmap* Convolution::conv2d_WS(fmap* input_features)
{
	int N = input_features->dim1;
	int H = input_features->dim3;
	int W = input_features->dim4;
  std::cout << " \n Conv_2d_WS Dimensions of ifmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  std::cout << " \n Conv_2d_WS Dimensions of weights \n \t M = " << M << "\t C = " << C << "\t R = " << R << "\t S = " << S << "\n";
	int E = ((H - R + 2*Px)/Sx) + 1;
	int F = ((W - S + 2*Py)/Sy) + 1;
	fmap* out_fmap = new fmap;
	out_fmap->dim1 = N;
	out_fmap->dim2 = M;
	out_fmap->dim3 = E;
	out_fmap->dim4 = F;
	out_fmap->data = (DATA*)calloc(N * M * E * F, sizeof(float));
	float (*temp_ofmap)[M][E][F] = (float (*)[M][E][F])out_fmap->data;
  float (*temp_weights)[C][R][S] = (float (*)[C][R][S])weights;
  float (*temp_ifmap)[C][H][W] = (float (*)[C][H][W])input_features->data;

	__m256i mem_mask_weight, mem_mask_ifmap;
	__m256 temp_1_ifmap, temp_1_weight, temp_out;
	int index_weight;
	int index_ifmap[8];

  clock_t start, end;
  start = clock();
  for(int m=0; m<M; m++)
		for(int c=0; c<C; c++)
  		for(int r=0; r<R; r++) 
  			for(int s=0; s<S; s=s+8) {
					index_weight = s-S; 
					mem_mask_weight = _mm256_set_epi32(index_weight+7,index_weight+6,index_weight+5,index_weight+4,index_weight+3,index_weight+2,index_weight+1,index_weight);
					temp_1_weight = _mm256_maskload_ps((float const*) &temp_weights[m][c][r][s], mem_mask_weight);
  				for(int n=0; n<N; n++)
  					for(int e=0; e<E; e++) {
  						int temp_r = e*Sx+r-Px;
							if((temp_r >= 0)&&(temp_r < E)) {
  							for(int f=0; f<F; f++) {
  								int temp_c = f*Sy+s-Py;
									for(int count=0; count<8; count++){
										if((f*Sy+s-Py+count >= 0)&&(f*Sy+s-Py+count < F))
											index_ifmap[count] = f - F;
										else
											index_ifmap[count] = F;
									}
									mem_mask_ifmap = _mm256_set_epi32(index_ifmap[7],index_ifmap[6],index_ifmap[5],index_ifmap[4],index_ifmap[3],index_ifmap[2],index_ifmap[1],index_ifmap[0]);
									temp_1_ifmap = _mm256_broadcast_ss((float const*) &temp_ifmap[n][c][temp_r][temp_c]);
									temp_out = _mm256_mul_ps(temp_1_weight, temp_1_ifmap);
									temp_out = _mm256_add_ps(temp_out,_mm256_maskload_ps((float const*) &temp_ofmap[n][m][e][f], mem_mask_ifmap));
									_mm256_maskstore_ps(&temp_ofmap[n][m][e][f],mem_mask_ifmap,temp_out);
								}
  						}
						}
  				}
  end = clock(); 
  exec_time = double(end-start) / double(CLOCKS_PER_SEC);
  std::cout << " \n Conv_2d_WS Dimensions of Ofmap \n \t N = " << N << "\t M = " << M << "\t E = " << E << "\t F = " << F << "\n" ;
	return out_fmap;
}

fmap* Convolution::conv2d_optimized(fmap* input_features, int tile_size_1, int tile_size_2)
{
	int N = input_features->dim1;
	int H = input_features->dim3;
	int W = input_features->dim4;
  std::cout << " \n Conv_2d_OS Dimensions of ifmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  std::cout << " \n Conv_2d_OS Dimensions of weights \n \t M = " << M << "\t C = " << C << "\t R = " << R << "\t S = " << S << "\n";
	int E = ((H - R + 2*Px)/Sx) + 1;
	int F = ((W - S + 2*Py)/Sy) + 1;
	fmap* out_fmap = new fmap;
	out_fmap->dim1 = N;
	out_fmap->dim2 = M;
	out_fmap->dim3 = E;
	out_fmap->dim4 = F;
	out_fmap->data = (DATA*)calloc(N * M * E * F, sizeof(float));
	float (*temp_ofmap)[M][E][F] = (float (*)[M][E][F])out_fmap->data;
  float (*temp_weights)[C][R][S] = (float (*)[C][R][S])weights;
  float (*temp_ifmap)[C][H][W] = (float (*)[C][H][W])input_features->data;

	__m256i mem_mask, mem_mask_ofmap;
	__m256 temp_1_ifmap, temp_1_weight, temp_out, temp_out_fmap;
	int index[8]; 
	int index_ofmap;
	
  clock_t start, end;
  start = clock();
	int te=tile_size_1;
	int tf=tile_size_2;
	int e=0,f=0;
  for(int n=0; n<N; n++)
  	for(int m=0; m<M; m++)
			for(int e1=0; e1<E; e1=e1+te)
  			for(int e2=0; e2<te; e2++)
					for(int f1=0; f1<F; f1=f1+tf) 
  					for(int f2=0; f2<tf; f2=f2+8) {
								f = f1*tf+f2;
								e = e1*te+e2;
								index_ofmap = f-F;
								mem_mask_ofmap = _mm256_set_epi32(index_ofmap+7, index_ofmap+6, index_ofmap+5, index_ofmap+4, index_ofmap+3, index_ofmap+2, index_ofmap+1, index_ofmap);
								temp_out_fmap = _mm256_loadu_ps((float const*) &temp_ofmap[n][m][e][f]);
  							for(int c=0; c<C; c++)
  							for(int r=0; r<R; r++) {
  								int temp_r = e*Sx+r-Px;
  								if((temp_r >= 0)&&(temp_r < E))
  									for(int s=0; s<S; s++) {
											int temp_c[8] = {(f+7)*Sy+s-Py, (f+6)*Sy+s-Py, (f+5)*Sy+s-Py, (f+4)*Sy+s-Py, (f+3)*Sy+s-Py, (f+2)*Sy+s-Py, (f+1)*Sy+s-Py, f*Sy+s-Py};
											for(int count=0; count<8; count++) {
												if(temp_c[count] >=0 && temp_c[count] < F)
													index[count] = s - S;
												else
													index[count] = S;
											}
											mem_mask = _mm256_set_epi32(index[7], index[6], index[5], index[4], index[3], index[2], index[1], index[0]);
											temp_1_weight = _mm256_broadcast_ss((float const*) &temp_weights[m][c][r][s]);
											temp_1_ifmap  = _mm256_set_ps(temp_ifmap[n][c][temp_r][temp_c[7]], temp_ifmap[n][c][temp_r][temp_c[6]], temp_ifmap[n][c][temp_r][temp_c[5]], temp_ifmap[n][c][temp_r][temp_c[4]],
																										temp_ifmap[n][c][temp_r][temp_c[3]], temp_ifmap[n][c][temp_r][temp_c[2]], temp_ifmap[n][c][temp_r][temp_c[1]], temp_ifmap[n][c][temp_r][temp_c[0]]);
											temp_out = _mm256_mul_ps(temp_1_weight, temp_1_ifmap);
											temp_out_fmap = _mm256_add_ps(temp_out, temp_out_fmap);
  									}
  							}
							_mm256_maskstore_ps(&temp_ofmap[n][m][e][f],mem_mask_ofmap,temp_out_fmap);
						}
  end = clock(); 
  exec_time = double(end-start) / double(CLOCKS_PER_SEC);
  std::cout << " \n Conv_2d_OS Dimensions of Ofmap \n \t N = " << N << "\t M = " << M << "\t E = " << E << "\t F = " << F << "\n" ;
	return out_fmap;
}

fmap* Linear::linear(fmap* input_features)
{
	int N = input_features->dim1;
	int C = input_features->dim2;
	int H = input_features->dim3;
	int W = input_features->dim4;
  DATA (*temp_ifmap)[C][H][W] = (DATA (*)[C][H][W])input_features->data;
  std::cout << " \n FCL Dimensions of ifmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  DATA (*temp_weights)[M] = (DATA (*)[M])weights;	
  std::cout << " \n FCL Dimensions of weights \n \t M = " << M << "\t L = " << L << "\n";
	
	fmap* out_fmap = new fmap;
	out_fmap->dim1 = N;
	out_fmap->dim2 = M;
	out_fmap->dim3 = 1;
	out_fmap->dim4 = 1;
	out_fmap->data = (DATA*)calloc(M, sizeof(DATA));
	DATA (*temp_ofmap)[M] = (DATA (*)[M])out_fmap->data;

	__m256i mem_mask;
	__m256 acc_out, temp_out, temp_1_weight, temp_1_ifmap;

  clock_t start, end;
  start = clock();
	for(int n=0; n<N; n++) {
		for(int m=0; m<M; m=m+8) {
			int index = m - M;
			mem_mask = _mm256_set_epi32(index+7, index+6, index+5, index+4, index+3, index+2, index+1, index);
			temp_out = _mm256_maskload_ps((float const*) &temp_ofmap[n][m], mem_mask);
			for(int l=0; l<L; l++) {
				temp_1_weight = _mm256_maskload_ps((float const*) &temp_weights[l][m], mem_mask);
				temp_1_ifmap = _mm256_broadcast_ss((float const*) &temp_ifmap[n][l][0][0]);
				acc_out = _mm256_mul_ps(temp_1_weight, temp_1_ifmap);
				temp_out = _mm256_add_ps(temp_out, acc_out);
			}
			_mm256_maskstore_ps(&temp_ofmap[n][m], mem_mask, temp_out);
		}
	}	
  end = clock(); 
  exec_time = double(end-start) / double(CLOCKS_PER_SEC);
  std::cout << " \n FCL Dimensions of ofmap \n \t M = " << M  << "\t N = " << N << "\n";
  return out_fmap;
}

fmap* Linear::linear_optimized(fmap* input_features)
{
  return NULL;
}

void relu(fmap* input_features)
{
  int N = input_features->dim1;
	int C = input_features->dim2;
	int H = input_features->dim3;
	int W = input_features->dim4;
  std::cout << " \n ReLU Dimensions of fmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  DATA (*temp_ifmap)[C][H][W] = (DATA (*)[C][H][W])input_features->data;

	float* addr = &temp_ifmap[0][0][0][0];

	__m256i mem_mask;
	__m256 fmap_val, cmp_out, cmp_val;
	int index;

	for(int count=0; count<((N*C*H*W)/8)+1; count++) {
		int index = count*8 - N*C*H*W;
		mem_mask = _mm256_set_epi32(index-7, index-6, index-5, index-4, index-3, index-2, index-1, index);
		cmp_val  = _mm256_set_ps(0, 0, 0, 0, 0, 0, 0, 0);
		fmap_val = _mm256_maskload_ps((float const*) addr, mem_mask);
		cmp_out = _mm256_cmp_ps(fmap_val,cmp_val,13);
		fmap_val = _mm256_and_ps(cmp_out, fmap_val);
		_mm256_maskstore_ps(addr, mem_mask, fmap_val);
		addr = addr + 8;
	}
}

fmap* maxpool_2d(fmap* input_features, int R, int S, int Sx, int Sy)
{
  int N = input_features->dim1;
	int C = input_features->dim2;
	int H = input_features->dim3;
	int W = input_features->dim4;
  std::cout << " \n MaxPool Dimensions of ifmap \n \t N = " << N << "\t C = " << C << "\t H = " << H << "\t W = " << W << "\n";
  DATA (*temp_ifmap)[C][H][W] = (DATA (*)[C][H][W])input_features->data;

	int E = ((H - R)/Sx) + 1;
	int F = ((W - S)/Sy) + 1;

	fmap* out_fmap = new fmap;
	out_fmap->dim1 = N;
	out_fmap->dim2 = C;
	out_fmap->dim3 = E;
	out_fmap->dim4 = F;
	out_fmap->data = (DATA*)calloc(N * C * E * F, sizeof(DATA));
	DATA (*temp_ofmap)[C][E][F] = (DATA (*)[C][E][F])out_fmap->data;
	
	int index_ofmap, index_ifmap, temp_r, temp_c;
	__m256i mem_mask_ofmap; 
	__m256 temp_1_ofmap, temp_1_ifmap;

	for(int n=0; n<N; n++)
		for(int c=0; c<C; c++)
			for(int e=0; e<E; e++)
				for(int f=0; f<F; f=f+8) {
					index_ofmap = f-F;
					mem_mask_ofmap = _mm256_set_epi32(index_ofmap+7, index_ofmap+6, index_ofmap+5, index_ofmap+4, index_ofmap+3, index_ofmap+2, index_ofmap+1, index_ofmap);
					temp_1_ofmap = _mm256_maskload_ps((float const*) &temp_ofmap[n][c][e][f], mem_mask_ofmap);
					for(int r=0; r<R; r++) {
						for(int s=0; s<S; s++) {
							temp_r = e*Sx;
							temp_c = f*Sy;
							temp_1_ifmap = _mm256_set_ps(temp_ifmap[n][c][temp_r+r][temp_c+7*Sy+s],temp_ifmap[n][c][temp_r+r][temp_c+6*Sy+s],temp_ifmap[n][c][temp_r+r][temp_c+5*Sy+s],temp_ifmap[n][c][temp_r+r][temp_c+4*Sy+s],
																							temp_ifmap[n][c][temp_r+r][temp_c+3*Sy+s],temp_ifmap[n][c][temp_r+r][temp_c+2*Sy+s],temp_ifmap[n][c][temp_r+r][temp_c+Sy+s],temp_ifmap[n][c][temp_r+r][temp_c+s]);
							temp_1_ofmap = _mm256_max_ps(temp_1_ofmap, temp_1_ifmap);
						}
					}
					_mm256_maskstore_ps(&temp_ofmap[n][c][e][f], mem_mask_ofmap, temp_1_ofmap);
				}
 
  std::cout << " \n MaxPool Dimensions of ofmap \n \t N = " << N << "\t C = " << C << "\t E = " << E << "\t F = " << F << "\n";

  return out_fmap;
}

AlexNet::AlexNet()
{
  conv_layers = (Convolution**) malloc(5 * sizeof(Convolution*));

  Convolution *conv;
  conv = new Convolution(96, 3, 11, 11, 4, 4, 2, 2);
  conv_layers[0] = conv;
  conv = new Convolution(256, 96, 5, 5, 1, 1, 2, 2);
  conv_layers[1] = conv;
  conv = new Convolution(384, 256, 3, 3, 1, 1, 1, 1);
  conv_layers[2] = conv;
  conv = new Convolution(384, 384, 3, 3, 1, 1, 1, 1);
  conv_layers[3] = conv;
  conv = new Convolution(256, 384, 3, 3, 1, 1, 1, 1);
  conv_layers[4] = conv;

  linear_layers = (Linear**) malloc(3 * sizeof(Linear*));
 
  Linear *linear;
  linear = new Linear(4096, 9216);
  linear_layers[0] = linear;
  linear = new Linear(4096, 4096);
  linear_layers[1] = linear;
  linear = new Linear(1000, 4096);
  linear_layers[2] = linear;
}

fmap* AlexNet::forward_pass(fmap* input_features, int tile_size_1, int tile_size_2)
{
  clock_t start, end;
  start = clock();

  fmap* temp = input_features;
  
  std::cout << " \n ****** CONVOLUTION LAYER 1 ***** \n";
  temp = conv_layers[0]->conv2d_OS(temp);
  relu(temp);
  temp = maxpool_2d(temp, 3, 3, 2, 2);
  std::cout << " \n ****** CONVOLUTION LAYER 2 ***** \n";
  temp = conv_layers[1]->conv2d_optimized(temp, tile_size_1, tile_size_2);
  relu(temp);
  temp = maxpool_2d(temp, 3, 3, 2, 2);
  std::cout << " \n ****** CONVOLUTION LAYER 3 ***** \n";
  temp = conv_layers[2]->conv2d_OS(temp);
  relu(temp);
  std::cout << " \n ****** CONVOLUTION LAYER 4 ***** \n";
  temp = conv_layers[3]->conv2d_OS(temp);
  relu(temp);
  std::cout << " \n ****** CONVOLUTION LAYER 5 ***** \n";
  temp = conv_layers[4]->conv2d_OS(temp);
  relu(temp);
  temp = maxpool_2d(temp, 3, 3, 2, 2);
 
  int lin_dim = temp->dim2 * temp->dim3 * temp->dim4;
  temp->dim2 = lin_dim;
  temp->dim3 = temp->dim4 = 1;
 
  std::cout << " \n ****** FULLY CONNECTED LAYER 1 ***** \n";
  temp = linear_layers[0]->linear(temp);
  relu(temp);
  std::cout << " \n ****** FULLY CONNECTED LAYER 2 ***** \n";
  temp = linear_layers[1]->linear(temp);
  relu(temp);
  std::cout << " \n ****** FULLY CONNECTED LAYER 3 ***** \n";
  temp = linear_layers[2]->linear(temp);
  relu(temp);
 
  end = clock();
 
  exec_time = double(end-start) / double(CLOCKS_PER_SEC);
  return temp;
}
